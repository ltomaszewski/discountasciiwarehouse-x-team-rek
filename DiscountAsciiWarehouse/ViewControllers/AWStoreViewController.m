//
//  ViewController.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "AWStoreViewController.h"
#import "AWItemDetailsViewController.h"

#import "AWApiClient.h"
#import "WarehouseItemDataSource.h"

#import "AWASCIIFaceCell.h"
#import "AWLoadingCell.h"

@interface AWStoreViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UITextFieldDelegate, WarehouseItemDataSourceDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *onlyInStockCheckbox;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@property (strong, nonatomic) AWApiClient *apiClient;
@property (strong, nonatomic) WarehouseItemDataSource *dataSource;
@property (assign, nonatomic) float optimalCellWidth;
@property (assign, nonatomic) BOOL showLoader;

@end

@implementation AWStoreViewController

- (IBAction)testbuttonAction:(id)sender {
    [_dataSource fetchMoreData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _showLoader = YES;
    _apiClient = [[AWApiClient alloc] init];
    _optimalCellWidth = [self findOptimalCellWidthForContainerWidth:[UIScreen mainScreen].bounds.size];
    
    _dataSource = [[WarehouseItemDataSource alloc] initWithApiClient:_apiClient];
    _dataSource.delegate = self;
    
    [_collectionView registerNib:[UINib nibWithNibName:[AWASCIIFaceCell classReuseIndentifier] bundle:nil]
      forCellWithReuseIdentifier:[AWASCIIFaceCell classReuseIndentifier]];
    
    [_collectionView registerNib:[UINib nibWithNibName:[AWLoadingCell classReuseIndentifier] bundle:nil]
      forCellWithReuseIdentifier:[AWLoadingCell classReuseIndentifier]];
}

-(float)findOptimalCellWidthForContainerWidth:(CGSize)screenSize {
    int numberOfItemsInRow = 2;
    if (screenSize.width > screenSize.height) {
        numberOfItemsInRow = 3;
    }
    return ((float)screenSize.width/numberOfItemsInRow);
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        _optimalCellWidth = [self findOptimalCellWidthForContainerWidth:size];
        [self.collectionView reloadData];
    } completion:nil];
}

- (IBAction)onlyInStockCheckBoxAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.dataSource.limitDataToInStockOnly = sender.selected;
}

#pragma mark - WarehouseItemDataSourceDelegate
#pragma mark -

- (void)warehouseDataSourceHasChanged:(WarehouseItemDataSource *)dataSource newItems:(NSArray *)newItems {
    NSArray *allItems = [dataSource allItems];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        _showLoader = (newItems.count != 0) || (allItems.count == 0);
        [self.collectionView reloadData];
    }];
}

-(void)warehouseDataSource:(WarehouseItemDataSource *)dataSource didReciveError:(NSError *)error {
}

#pragma mark - UICollectionViewDataSource
#pragma mark -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSource numberOfItems]+(_showLoader?1:0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [self.dataSource numberOfItems]) {
        AWASCIIFaceCell *faceCell = [collectionView dequeueReusableCellWithReuseIdentifier:[AWASCIIFaceCell classReuseIndentifier] forIndexPath:indexPath];
        faceCell.item = [self.dataSource itemAtIndex:indexPath.row];
        faceCell.layoutMargins = UIEdgeInsetsZero;
        return faceCell;
    } else {
        AWLoadingCell *loadingCell = [collectionView dequeueReusableCellWithReuseIdentifier:[AWLoadingCell classReuseIndentifier] forIndexPath:indexPath];
        [self.dataSource fetchMoreData];
        return loadingCell;
    }
}

#pragma mark - UICollectionViewDelegate
#pragma mark -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    AWItemDetailsViewController *itemDetailsViewController = [[AWItemDetailsViewController alloc] init];
    itemDetailsViewController.item = [self.dataSource itemAtIndex:indexPath.row];
    [self.navigationController pushViewController:itemDetailsViewController animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(_optimalCellWidth, _optimalCellWidth*0.8);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.searchTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate
#pragma mark - 

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.searchTextField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.dataSource.searchQuery = textField.text;
    [self.searchTextField resignFirstResponder];
}

@end

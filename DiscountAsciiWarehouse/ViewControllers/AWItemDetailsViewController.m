//
//  AWItemDetailsViewController.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 16/09/2016.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "AWItemDetailsViewController.h"
#import "AWWarehouseItem.h"

@interface AWItemDetailsViewController ()

@property (nonatomic, strong) UILabel *itemLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIButton *buyButton;

@end

@implementation AWItemDetailsViewController

- (void)loadView {
    UIView *view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    view.backgroundColor = [UIColor whiteColor];
    [view addSubview:self.itemLabel];
    [view addSubview:self.priceLabel];
    [view addSubview:self.buyButton];
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Item details", nil);
    self.itemLabel.text = self.item.face;
    self.priceLabel.text = [NSString stringWithFormat:@"$ %.2f",(self.item.price.floatValue/100)];;
    [self.buyButton setTitle:NSLocalizedString(@"Buy it now!", nil) forState:UIControlStateNormal];
    [self.buyButton setNeedsUpdateConstraints];
    [self.view setNeedsUpdateConstraints];
}

#pragma mark - Actions
#pragma mark -

- (void)buyAction {
    UIAlertController *simpleAlertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Hello", nil)
                                                                                   message:NSLocalizedString(@"You have just bought Ascii smile face. Congrats !!!", nil)
                                                                            preferredStyle:UIAlertControllerStyleAlert];
    [simpleAlertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cool", nil)
                                                              style:UIAlertActionStyleCancel
                                                            handler:nil]];
    [self presentViewController:simpleAlertController animated:YES completion:nil];
}

#pragma mark - Layout
#pragma mark -

- (void)updateViewConstraints {
    [self setupItemLabelConstraints];
    [self setupPriceLabelConstraints];
    [self setupBuyButtonConstraints];
    [super updateViewConstraints];
}

- (void)setupItemLabelConstraints {
    UILabel *itemLabel = self.itemLabel;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:itemLabel
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:100.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:itemLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
}

- (void)setupPriceLabelConstraints {
    UILabel *priceLabel = self.priceLabel;
    UILabel *itemLabel = self.itemLabel;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:priceLabel
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:itemLabel
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:10.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:priceLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:itemLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
}

- (void)setupBuyButtonConstraints {
    UILabel *priceLabel = self.priceLabel;
    UIButton *button = self.buyButton;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:priceLabel
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:1.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:priceLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
}


#pragma mark - UI Getters
#pragma mark -

- (UILabel *)itemLabel {
    if (!_itemLabel) {
        _itemLabel = [[UILabel alloc] init];
        _itemLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _itemLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _priceLabel;
}

- (UIButton *)buyButton {
    if (!_buyButton) {
        _buyButton = [[UIButton alloc] init];
        [_buyButton setBackgroundColor:[UIColor redColor]];
        [_buyButton addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
        _buyButton.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _buyButton;
}

@end

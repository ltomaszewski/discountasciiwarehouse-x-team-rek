//
//  AWItemDetailsViewController.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 16/09/2016.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AWWarehouseItem;

@interface AWItemDetailsViewController : UIViewController

@property (strong, nonatomic) AWWarehouseItem *item;

@end

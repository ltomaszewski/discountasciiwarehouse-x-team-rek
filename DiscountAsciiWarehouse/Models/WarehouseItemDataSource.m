//
//  WarehouseItemDataSource.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "WarehouseItemDataSource.h"
#import "AWApiClient.h"
#import "AWWarehouseItem.h"

static NSInteger const kDefaultSearchQueryLimit = 10;

@interface WarehouseItemDataSource()

@property (strong, nonatomic) NSMutableArray *items;
@property (strong, nonatomic) NSURLSessionDataTask *currentDataTask;
@property (strong, nonatomic) NSOperationQueue *updateItemsOperationQueue;
@property (weak, nonatomic) AWApiClient *apiClient;

@end

@implementation WarehouseItemDataSource

- (instancetype)initWithApiClient:(AWApiClient *)apiClient
{
    if (self = [super init]) {
        _items = [NSMutableArray array];
        _apiClient = apiClient;
        _updateItemsOperationQueue = [[NSOperationQueue alloc] init];
        _updateItemsOperationQueue.maxConcurrentOperationCount = 1;
    }
    return self;
}

- (NSInteger)numberOfItems {
    return [_items count];
}

- (AWWarehouseItem *)itemAtIndex:(NSInteger)index {
    return _items[index];
}

- (BOOL)fetchMoreData {
    if (_currentDataTask && _currentDataTask.state == NSURLSessionTaskStateRunning) {
        return false;
    }
    
    _currentDataTask = [_apiClient searchWithQuery:_searchQuery
                                             limit:kDefaultSearchQueryLimit
                                              skip:_items.count
                                       onlyInStock:_limitDataToInStockOnly
                                   completionBlock:^(NSArray *items, NSError *error) {
                                       if (error && [_delegate respondsToSelector:@selector(warehouseDataSource:didReciveError:)]) {
                                               [_delegate warehouseDataSource:self didReciveError:error];
                                       } else {
                                           NSInvocationOperation *updateItemsOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(updateItemsWith:) object:items];
                                           [_updateItemsOperationQueue addOperation:updateItemsOperation];
                                       }
                                   }];
    return true;
}

- (void)updateItemsWith:(NSArray *)newItems {
    [_items addObjectsFromArray:newItems];
    if ([_delegate respondsToSelector:@selector(warehouseDataSourceHasChanged:newItems:)]) {
        [_delegate warehouseDataSourceHasChanged:self newItems:newItems];
    }
}

#pragma mark - Getter & Setters
#pragma mark -

- (void)setSearchQuery:(NSString *)searchQuery {
    if (![_searchQuery isEqualToString:searchQuery]) {
        _searchQuery = searchQuery;
        
        if (_searchQuery.length == 0) {
            _searchQuery = nil;
        }
        [self resetData];
    }
}

- (void)setLimitDataToInStockOnly:(BOOL)limitDataToInStockOnly {
    if (_limitDataToInStockOnly != limitDataToInStockOnly) {
        _limitDataToInStockOnly = limitDataToInStockOnly;
        [self resetData];
    }
}

- (void)resetData {
    [_currentDataTask cancel];
    [_items removeAllObjects];
    [_updateItemsOperationQueue cancelAllOperations];
    if ([_delegate respondsToSelector:@selector(warehouseDataSourceHasChanged:newItems:)]) {
        [_delegate warehouseDataSourceHasChanged:self newItems:nil];
    }
}

- (NSArray *)allItems {
    return _items.copy;
}

@end

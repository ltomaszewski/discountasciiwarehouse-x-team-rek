//
//  WarehouseItem.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <Mantle.h>

@interface AWWarehouseItem : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *type;
@property (nonatomic, copy, readonly) NSString *identifier;
@property (nonatomic, copy, readonly) NSNumber *price;
@property (nonatomic, copy, readonly) NSNumber *size;
@property (nonatomic, copy, readonly) NSString *face;
@property (nonatomic, copy, readonly) NSNumber *stock;
@property (nonatomic, copy, readonly) NSArray *tags;

@end

//
//  WarehouseItemDataSource.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AWWarehouseItem, WarehouseItemDataSource,AWApiClient;

@protocol WarehouseItemDataSourceDelegate <NSObject>

- (void)warehouseDataSourceHasChanged:(WarehouseItemDataSource *)dataSource newItems:(NSArray *)newItems;
- (void)warehouseDataSource:(WarehouseItemDataSource *)dataSource didReciveError:(NSError *)error;

@end

@interface WarehouseItemDataSource : NSObject

@property (strong, nonatomic) NSString *searchQuery;
@property (assign, nonatomic) BOOL limitDataToInStockOnly;
@property (strong, nonatomic, readonly) NSArray *allItems;
@property (weak, nonatomic) id<WarehouseItemDataSourceDelegate> delegate;

- (instancetype)initWithApiClient:(AWApiClient *)apiClient;
- (AWWarehouseItem *)itemAtIndex:(NSInteger)index;
- (NSInteger)numberOfItems;
- (BOOL)fetchMoreData;

@end

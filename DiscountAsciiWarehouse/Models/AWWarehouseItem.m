//
//  WarehouseItem.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "AWWarehouseItem.h"

@implementation AWWarehouseItem

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"type" : @"type",
             @"identifier" : @"id",
             @"size" : @"size",
             @"price" : @"price",
             @"face" : @"face",
             @"stock" : @"stock",
             @"tags" : @"tags"
             };
}

@end

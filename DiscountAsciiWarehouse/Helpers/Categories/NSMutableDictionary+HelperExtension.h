//
//  NSMutableDictionary+HelperExtension.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (HelperExtension)

- (void)HE_setObjectIfNotNil:(id)object forKey:(id<NSCopying>)key;

@end

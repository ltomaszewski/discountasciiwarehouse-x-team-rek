//
//  UIButton+AWBackgroundColor.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 28/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (AWBackgroundColor)

- (void)BC_setBackgroundColor:(UIColor *)color forState:(UIControlState)state;

@end

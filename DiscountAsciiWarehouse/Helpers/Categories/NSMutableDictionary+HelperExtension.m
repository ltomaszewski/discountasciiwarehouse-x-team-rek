//
//  NSMutableDictionary+HelperExtension.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "NSMutableDictionary+HelperExtension.h"

@implementation NSMutableDictionary (HelperExtension)

- (void)HE_setObjectIfNotNil:(id)object forKey:(id<NSCopying>)key {
    if (object) {
        [self setObject:object forKey:key];
    }
}

@end

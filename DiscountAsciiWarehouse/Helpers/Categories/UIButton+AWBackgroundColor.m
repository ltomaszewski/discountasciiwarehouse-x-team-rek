//
//  UIButton+AWBackgroundColor.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 28/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "UIButton+AWBackgroundColor.h"

@implementation UIButton (AWBackgroundColor)

- (void)BC_setBackgroundColor:(UIColor *)color forState:(UIControlState)state
{
    [self setBackgroundImage:[self imageWithColor:color] forState:state];
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end

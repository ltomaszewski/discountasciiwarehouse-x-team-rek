//
//  SimpleURLCache.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 12/02/15.
//  Copyright © 2015 Lukasz Tomaszewski. All rights reserved.
//

#import "LTSimpleURLCache.h"

static NSString * const kSimpleURLCacheExpirationKey = @"kSimpleURLCacheExpirationKey";
static NSTimeInterval const kSimpleURLCacheDefaultExpirationInterval = 60;

@interface LTSimpleURLCache()

@property (nonatomic, assign) NSTimeInterval cacheExpirationInterval;

@end

@implementation LTSimpleURLCache

- (instancetype)initWithMemoryCapacity:(NSUInteger)memoryCapacity
                          diskCapacity:(NSUInteger)diskCapacity
                              duration:(NSTimeInterval)duration
{
    if ( self = [super initWithMemoryCapacity:memoryCapacity
                                diskCapacity:diskCapacity
                                    diskPath:nil] )
    {
        self.cacheExpirationInterval = duration;
    }
    
    return self;
}

#pragma mark - NSURLCache

- (NSCachedURLResponse *)cachedResponseForRequest:(NSURLRequest *)request
{
    NSCachedURLResponse *cachedResponse = [super cachedResponseForRequest:request];
    
    if ( cachedResponse )
    {
        NSDate* cacheDate = cachedResponse.userInfo[kSimpleURLCacheExpirationKey];
        NSDate* cacheExpirationDate = [cacheDate dateByAddingTimeInterval:self.cacheExpirationInterval];
        if ( [cacheExpirationDate compare:[NSDate date]] == NSOrderedAscending )
        {
            [self removeCachedResponseForRequest:request];
            return nil;
        }
    }
    
    
    return cachedResponse;
}

- (void)storeCachedResponse:(NSCachedURLResponse *)cachedResponse
                 forRequest:(NSURLRequest *)request
{
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:cachedResponse.userInfo];
    userInfo[kSimpleURLCacheExpirationKey] = [NSDate date];
    
    NSCachedURLResponse *modifiedCachedResponse = [[NSCachedURLResponse alloc] initWithResponse:cachedResponse.response
                                                                                           data:cachedResponse.data
                                                                                       userInfo:userInfo
                                                                                  storagePolicy:cachedResponse.storagePolicy];

    [super storeCachedResponse:modifiedCachedResponse forRequest:request];
}

- (NSTimeInterval)cacheExpirationInterval {
    
    if ( _cacheExpirationInterval == 0 )
    {
        _cacheExpirationInterval = kSimpleURLCacheDefaultExpirationInterval;
    }
    
    return _cacheExpirationInterval;
}


@end

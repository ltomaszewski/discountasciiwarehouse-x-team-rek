//
//  AWXJSONResponseSerializer.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface LTNDJSONResponseSerializer : AFJSONResponseSerializer

@end

//
//  LTNDJSONResponseSerializer.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "LTNDJSONResponseSerializer.h"

@implementation LTNDJSONResponseSerializer

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.acceptableContentTypes = [NSSet setWithObjects:@"application/x-json-stream", nil];
    
    return self;
}

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
    if (![self validateResponse:(NSHTTPURLResponse *)response data:data error:error])
    {
        if (!error)
        {
            return nil;
        }
    }
    
    // Workaround for behavior of Rails to return a single space for `head :ok` (a workaround for a bug in Safari), which is not interpreted as valid input by NSJSONSerialization.
    // See https://github.com/rails/rails/issues/1742
    NSStringEncoding stringEncoding = self.stringEncoding;
    if (response.textEncodingName)
    {
        CFStringEncoding encoding = CFStringConvertIANACharSetNameToEncoding((CFStringRef)response.textEncodingName);
        if (encoding != kCFStringEncodingInvalidId)
        {
            stringEncoding = CFStringConvertEncodingToNSStringEncoding(encoding);
        }
    }
    
    __block NSError *serializationError = nil;
    __block NSData *localData = data;
    __block id responseObject = nil;
    __block NSMutableArray *result = [NSMutableArray array];
    @autoreleasepool {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:stringEncoding];

        if ( responseString && ![responseString isEqualToString:@" "] )
        {
            // Workaround for a bug in NSJSONSerialization when Unicode character escape codes are used instead of the actual character
            // See http://stackoverflow.com/a/12843465/157142
            
            [responseString enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
                localData = [line dataUsingEncoding:NSUTF8StringEncoding];
                if ( localData )
                {
                    if ( [localData length] > 0 )
                    {
                        responseObject = [NSJSONSerialization JSONObjectWithData:localData options:self.readingOptions error:&serializationError];
                        if ( serializationError )
                        {
                            *error = serializationError;
                        }
                        else
                        {
                            [result addObject:responseObject];
                        }
                    }
                }
            }];
        }
    }
    
    return result.copy;
}

@end

//
//  SimpleURLCache.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LTSimpleURLCache : NSURLCache

- (instancetype)initWithMemoryCapacity:(NSUInteger)memoryCapacity
                          diskCapacity:(NSUInteger)diskCapacity
                              duration:(NSTimeInterval)duration;

@end

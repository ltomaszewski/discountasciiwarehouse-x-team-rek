//
//  AWApiClient.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <AFHTTPSessionManager.h>

@class AWWarehouseItem;

@interface AWApiClient : AFHTTPSessionManager

- (NSURLSessionDataTask *)searchWithQuery:(NSString *)query
                                    limit:(NSInteger)limit
                                     skip:(NSInteger)skip
                              onlyInStock:(BOOL)onlyInStock
                          completionBlock:(void(^)(NSArray* items, NSError *error))completionBlock;

@end

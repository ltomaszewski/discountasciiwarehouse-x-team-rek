//
//  AWApiClient.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "AWApiClient.h"
#import <AFNetworking.h>
#import <Mantle.h>
#import "LTNDJSONResponseSerializer.h"
#import "AWWarehouseItem.h"
#import "NSMutableDictionary+HelperExtension.h"

//TODO: add to note to add SSL support to meet new connection restriction by iOS 9
static NSString *const kAPIBaseURLString = @"http://74.50.59.155:5000/api/";
static NSString *const kAPIMethodSearch = @"search";

@implementation AWApiClient

- (instancetype)init
{
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.requestCachePolicy = NSURLRequestReturnCacheDataElseLoad;
    
    if ( self = [super initWithBaseURL:[NSURL URLWithString:kAPIBaseURLString]
                  sessionConfiguration:sessionConfiguration] )
    {
        self.responseSerializer = [LTNDJSONResponseSerializer serializer];
    }
    return self;
}

- (NSURLSessionDataTask *)searchWithQuery:(NSString *)query
                                    limit:(NSInteger)limit
                                     skip:(NSInteger) skip
                              onlyInStock:(BOOL)onlyInStock
                          completionBlock:(void (^)(NSArray *, NSError *))completionBlock
{
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters HE_setObjectIfNotNil:query forKey:@"query"];
    [parameters HE_setObjectIfNotNil:@(limit) forKey:@"limit"];
    [parameters HE_setObjectIfNotNil:@(skip) forKey:@"skip"];
    [parameters HE_setObjectIfNotNil:@(onlyInStock) forKey:@"onlyInStock"];
    
    return [self GET:kAPIMethodSearch
          parameters:parameters
             success:^(NSURLSessionTask *task, id responseObject) {
                 NSError *error;
                 NSArray *items = [MTLJSONAdapter modelsOfClass:[AWWarehouseItem class] fromJSONArray:responseObject error:&error];
                 if (completionBlock) {
                     completionBlock(items,error);
                 }
             } failure:^(NSURLSessionTask *operation, NSError *error) {
                 if (completionBlock) {
                     completionBlock(nil,error);
                 }
             }];
}

@end

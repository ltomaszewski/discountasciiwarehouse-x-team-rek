//
//  AWASCIIFaceCell.h
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AWWarehouseItem;

@interface AWASCIIFaceCell : UICollectionViewCell

@property (strong, nonatomic) AWWarehouseItem *item;

+ (NSString *)classReuseIndentifier;

@end

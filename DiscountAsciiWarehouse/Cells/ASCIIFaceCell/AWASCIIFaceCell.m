//
//  AWASCIIFaceCell.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "AWASCIIFaceCell.h"
#import "AWWarehouseItem.h"
#import "UIButton+AWBackgroundColor.h"

@interface AWASCIIFaceCell()

@property (weak, nonatomic) IBOutlet UILabel *faceLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyNowButton;

@end

@implementation AWASCIIFaceCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [_buyNowButton BC_setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_buyNowButton BC_setBackgroundColor:[UIColor redColor] forState:UIControlStateHighlighted];
    [_buyNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_buyNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}

- (void)setItem:(AWWarehouseItem *)item
{
    _item = item;
    _faceLabel.text = item.face;
    _priceLabel.text = [NSString stringWithFormat:@"$ %.2f",(item.price.floatValue/100)];
    [self refreshBuyNowButtonTitle];
}

- (void)refreshBuyNowButtonTitle
{
    if ( _item.stock.intValue == 1 ) {
        NSString *firstLine = @"BUY NOW!";
        NSString *secondLine = @"(Only 1 more in stock)";
        NSString *fullText = [NSString stringWithFormat:@"%@\n%@",firstLine,secondLine];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
        [paragraphStyle setAlignment:NSTextAlignmentCenter];
        
        NSMutableAttributedString *attributedBuyNowTitle = [[NSMutableAttributedString alloc] initWithString:fullText
                                                                                                  attributes:@{
                                                                                                               NSForegroundColorAttributeName:[UIColor whiteColor]
                                                                                                               }];
        [attributedBuyNowTitle addAttribute:NSFontAttributeName
                                      value:[UIFont systemFontOfSize:15]
                                      range:[fullText rangeOfString:firstLine]];
        [attributedBuyNowTitle addAttribute:NSFontAttributeName
                                      value:[UIFont systemFontOfSize:12]
                                      range:[fullText rangeOfString:secondLine]];
        [attributedBuyNowTitle addAttribute:NSParagraphStyleAttributeName
                                      value:paragraphStyle
                                      range:NSMakeRange(0, [fullText length])];
        
        [_buyNowButton setAttributedTitle:attributedBuyNowTitle
                                 forState:UIControlStateNormal];
    } else {
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"BUY NOW!" attributes:@{
                                                                                                                   NSForegroundColorAttributeName:[UIColor whiteColor]                                                                                                                   }];
        [_buyNowButton setAttributedTitle:attributedString
                                 forState:UIControlStateNormal];
    }
}


+ (NSString *)classReuseIndentifier
{
    return NSStringFromClass(self);
}

@end

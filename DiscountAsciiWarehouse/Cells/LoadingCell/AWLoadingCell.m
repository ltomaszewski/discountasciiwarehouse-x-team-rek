//
//  AWLoadingCell.m
//  DiscountAsciiWarehouse
//
//  Created by Lukasz Tomaszewski on 27/03/16.
//  Copyright © 2016 Lukasz Tomaszewski. All rights reserved.
//

#import "AWLoadingCell.h"

@implementation AWLoadingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (NSString *)classReuseIndentifier {
    return NSStringFromClass(self);
}


@end
